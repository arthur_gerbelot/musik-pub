### Description ###

Basic ready to start NodeJs server config :

 * Express
 * React
 * Grunt
 * Broserify
 * Reactify
 * Lodash
 * URIjs
 * Backbone 
 * jQuery
 * Jade
 * Foundation
 * Font-awesome
 * Heroku pre-configured 

### How to start ###
- Clone git repo : `git clone https://{your-login-here}@bitbucket.org/arthur_gerbelot/nodejs-grunt-broserify-react.git`
- Go on the folder : `cd nodejs-grunt-broserify-react`
- Install all package : `npm i`
- Start the server : `grunt dev`
- Go watch the result : `http://localhost:3500` (or other data on `config/app.js`)

### Missing parts ###
 
 * React JSX transform work with node-jsx on each file who require a component
 * jQuery (and foundation who have dependencies) without basics assets.
 * Allow multiple env (dev, staging, production)
 * Create an API side, and a remove all database part from here