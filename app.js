var http            = require('http');
var express         = require('express');
var jade            = require('jade');
var favicon         = require('serve-favicon');
var colors          = require('colors');
var session         = require('express-session');
var CookieParser    = require('cookie-parser');
var bodyParser      = require('body-parser');
var URI             = require('URIjs');

var config          = require('./config/app');
var routes          = require('./routes');

var cookieParser    = CookieParser(config.SESSION.KEY)

// Start server
var app = module.exports = express();

// Configure app
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.engine('.html', jade.__express);

// Public directory
app.use(express.static(__dirname + '/public'));

// Handle Favicon request
app.use(favicon(__dirname + '/assets/img/favicon.ico'));

// Set session
app.use(cookieParser);
// Set bodyParser
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded()); // to support URL-encoded bodies


app.use(function(req, res, next) {
  var API_URL;
  var PUBLIC_URL;

  switch(process.env.NODE_ENV) {
    case 'production':
      API_URL = '//' + process.env.API_URL;
      PUBLIC_URL = '//' + process.env.PUBLIC_URL;
      break;
    default:
      var host = new URI('//' + req.headers.host);
      API_URL = '//' + host.hostname() + ':' + config.API_PORT;
      PUBLIC_URL = '//' + host.hostname() + ':' + config.PORT;
      break;
  }
  // Setup req
  req.API_URL = API_URL;
  req.PUBLIC_URL = PUBLIC_URL;
  // Setup in Local
  res.locals.API_URL = API_URL;
  res.locals.PUBLIC_URL = PUBLIC_URL;

  // req.api = new api({ API_URL: API_URL });

  next();
});

// Setup routes
routes.setup(app);

// Handling error 404
app.use(function(req, res) {
  console.log("Error 404. Nothing for " + req.method + " " + req.url);
  res.send("Hum.. 404 Here. Nothing for " + req.method + " " + req.url, 404);
});

// Handling error 500
app.use(function(err, req, res, next) {
  console.log("Error 500.", err.stack);
  res.send("Hum.. An 500 error are occured for " + req.method + " " + req.url, 500);
});

process.on('uncaughtException', function(err) {
  console.log("Error 500 throw here. Process crash.")
  console.log(err.stack);
});

// Start server
var server = app.listen(process.env.PORT || config.PORT, function() {

  var host = server.address().address
  var port = server.address().port

  // Display
  for(var i=0; i<25; i++) {
    console.log(" "); // Clear shell
  }
  console.log('                               ' + colors.gray('8888'));
  console.log('                              ' + colors.gray('88') + '  ' + colors.gray('888'));
  console.log('                             ' + colors.gray('88') + '    ' + colors.gray('8888'));
  console.log('                            ' + colors.gray('88') + '       ' + colors.gray('8888'));
  console.log('                           ' + colors.gray('88') + '         ' + colors.gray('888'));
  console.log('                          ' + colors.gray('88') + '         ' + colors.gray('88888'));
  console.log('                         ' + colors.gray('88') + '        ' + colors.gray('88888888'));
  console.log('                        ' + colors.gray('888'));
  console.log('                       ' + colors.gray('888'));
  console.log('                      ' + colors.gray('888'));
  console.log('                     ' + colors.gray('888'));
  console.log('            ' + colors.gray('88888') + '   ' + colors.gray('888'));
  console.log('       ' + colors.gray('888888888888888'));
  console.log('     ' + colors.gray('888888888888888888'));
  console.log('    ' + colors.gray('8888888888888888888'));
  console.log('   ' + colors.gray('8888888888888888888') + '        ' + colors.green('Server listen at'));
  console.log('   ' + colors.gray('888888888888888888') + '         ' + colors.green('http://' + host + ':' + port));
  console.log('    ' + colors.gray('888888888888888'));
  console.log('        ' + colors.gray('888888'));
  for(var i=0; i<3; i++) {
    console.log(" "); // Clear shell
  }
});