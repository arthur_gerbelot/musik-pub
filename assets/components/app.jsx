var _      = require('lodash');
var $      = require('jquery');
var React  = require('react');

var MainApp = React.createClass({

  render: function() {

    return (
      <div id='react-app'>
        Main
      </div>
    );
  }
});

module.exports = MainApp;