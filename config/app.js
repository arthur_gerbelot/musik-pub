module.exports = {
  API_PORT: 3000,
  PORT: 3002,

  SESSION: {
    KEY: 'Musik++0.2SessionHackKey',
    TIMEOUT: 3600000, // 1h
  }
}