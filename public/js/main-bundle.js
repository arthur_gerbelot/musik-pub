(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var _      = require('lodash');
var $      = require('jquery');
var React  = require('react');

var MainApp = React.createClass({displayName: "MainApp",

  render: function() {

    return (
      React.createElement("div", {id: "react-app"}, 
        "Main"
      )
    );
  }
});

module.exports = MainApp;

},{"jquery":"jquery","lodash":"lodash","react":"react"}],2:[function(require,module,exports){
(function (global){
var React = require('react');

var MainApp = require('../components/app.jsx');

global.MainController = new function() {
  var self = this;

  this.react_data = {}; // This will be updated by Controller react data on /views/layout.jade

  this.init = function() {
    // Re Render to bind events
    self.reRender();
  };

  // ReRender React components
  this.reRender = function() {
    var App = React.createElement(MainApp, self.react_data);
    React.render(
      App,
      document.getElementById('react-app')
    );
  }
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"../components/app.jsx":1,"react":"react"}]},{},[2]);
